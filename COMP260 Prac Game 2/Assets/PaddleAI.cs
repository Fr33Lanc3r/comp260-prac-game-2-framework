﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PaddleAI : MonoBehaviour {

    private float speed = 10f;
    private Rigidbody rigidbody;
    private Transform pucktarget;
    public Transform goalown;

	void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;

        PuckControl puck = (PuckControl)FindObjectOfType(typeof(PuckControl));
        pucktarget = puck.transform;

        //goalown = GameObject.Find("Blue Goal").transform;
    }
	
	void Update () {
        //move the paddle
        //Vector3 direction;
        //direction.x = (goalown.position.x - pucktarget.position.x) /2;
        //direction.y = 0;
        //direction.z = (goalown.position.z - pucktarget.position.z) /2;

        //direction = direction.normalized;
        //Vector3 vel = direction * speed;

        //rigidbody.velocity = vel;

        transform.position = Vector3.Lerp(pucktarget.transform.position, goalown.transform.position, 0.5f);

        //Debug.Log("position: " + rigidbody.position);

    }
}
