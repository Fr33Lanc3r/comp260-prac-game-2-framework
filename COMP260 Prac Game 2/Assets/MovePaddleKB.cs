﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddleKB : MonoBehaviour
{

    private Rigidbody rigidbody;
    public float speed = 10f;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    void FixedUpdate()
    {
        Vector3 direction;
        direction.x = Input.GetAxis("Horizontal");
        direction.y = 0;
        direction.z = Input.GetAxis("Vertical");
        direction = direction.normalized;
        Vector3 vel = direction * speed;

        rigidbody.velocity = direction * speed;
    }
}
